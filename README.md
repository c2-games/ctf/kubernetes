# Kubernetes 
This repo holds Kubernetes deployment files for the NCAE Competition

File are broken down as follows:
2022/regionals/2022-02-12-deployments/vce1/

/<Competition Year> (YYYY)
|_/<Type> (regionals/finals)
 |_/<Date> (YYYY-MM-DD)
   |_/<Deployment Server> (vce1/vce2/...)
    |_/<Challenge YAML Files> (ctf-challenge.yaml)

